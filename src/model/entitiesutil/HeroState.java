package model.entitiesutil;

/**
 * Every possible state for {@link Hero}.
 */
public enum HeroState {

    /**
     * Normal state.
     */
    NORMAL,

    /**
     * Shooting state.
     */
    SHOOTING;
}
